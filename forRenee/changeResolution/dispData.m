function [] = dispData(data)
for i=1:size(data,3)
    imagesc(data(:,:,i))
    drawnow;
    pause(0.1)
end
end

