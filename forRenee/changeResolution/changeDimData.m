function [dataInterpolated] = changeDimData(thedata,newDimensions)
%CHANGEDIMDATA interpolate data into newDimensions
% Create a mesh which results in the final resolution(newDimensions)
originalDim=size(thedata);
ratioOfDims=double(originalDim-1)./double(newDimensions-1);
% Have to flip the X and Y because of the matlab weirdness (X,Y vs row,Col)
[Yq,Xq,Zq]=ndgrid(1:ratioOfDims(1):originalDim(1)+0.001,...
    1:ratioOfDims(2):originalDim(2)+0.001,...
    1:ratioOfDims(3):originalDim(3)+0.001);
% Use nearest neighbor method to interpolate
dataInterpolated=interp3(thedata,Xq,Yq,Zq,'nearest');
end

