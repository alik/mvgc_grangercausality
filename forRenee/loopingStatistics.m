clear;
load('Statistics.mat')
field = fieldnames(Statistics)';
subfields = fieldnames(Statistics.(field{1}))';
ROI = fieldnames(Statistics.(field{1}).(subfields{1}))';

% The loop
for R = 1:length(ROI)
    for sJ = 1:length(subfields)
        curStat=Statistics.(field{1}).(subfields{sJ}).(ROI{R}).statistic;
        % Here's the time first (change to 300)
        curStat_upSampled(:,1)=0.5:0.5:600;
        curStat_upSampled(:,2)= interp1(curStat(:,1),curStat(:,2),...
            curStat_upSampled(:,1),'linear','extrap');
        Statistics.(field{1}).(subfields{sJ}).(ROI{R}).statistic=...
            curStat_upSampled;
    end
end
% Plotting the data
subplot(2,1,1)
plot(curStat(:,1),curStat(:,2))
subplot(2,1,2)
plot(curStat_upSampled(:,1),curStat_upSampled(:,2))