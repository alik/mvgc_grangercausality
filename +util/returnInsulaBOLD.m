function [allBOLD,insulaROILabels]=returnInsulaBOLD()
% Author: Ali Karimi <ali.karimi@brain.mpg.de>
% OUTPUT:
%        allBOLD: Table
%        Variable names are animal IDs, within each variable a double array
%        containing the bold signal with the following dimensiions:
%           1. time, 2. insula ROI, 3. sessionID 
% Set up
% Load data
param=util.returnParam();
load(fullfile(param.dataDir,'allData.mat'));
% Get ROI names
insulaROIIndices = [16:18, 35, 48:49, 65:67, 84, 97:98];
ROInames=fieldnames(allData.A09GB1.Session_28);
insulaROInames=ROInames(insulaROIIndices);
insulaROILabels=cellfun(@(x) strrep(x,'_',' '),insulaROInames,...
    'UniformOutput',false);
% Get animal ID(Name)s
animalNames=fieldnames(allData);
% Get the Session IDs
sessionNames=structfun(@fieldnames,allData,'UniformOutput',false);
% Get the onsets of trials within each session
onSets=1:40:600;
% Initialize the table for all the bold signals per animal
allBOLD=util.createEmptyTable('cell',animalNames,{'allBoldData'});
% Fill out the table
for aID=1:length(animalNames)
    curAnimal=animalNames{aID};
    % Matrix containing all the bold signal from the same animal with
    % dimensions corresponding to (time,insularROI,recordingSession)
    curAnimalBOLD=zeros(600,length(insulaROInames),...
    length(sessionNames.(curAnimal)));
    % Iterate over the sessions and Insula ROIs here
    for sID=1:length(sessionNames.(curAnimal))
        curSession=sessionNames.(curAnimal){sID};
        for rID=1:length(insulaROInames)
            curROI=insulaROInames{rID};
            curBOLD=allData.(curAnimal).(curSession). ...
                (curROI).statistic(:,2);
            assert(length(curBOLD)==600);
            curAnimalBOLD(:,rID,sID)=...
                curBOLD;
        end
    end
    % Set the bold to a specific animal
    allBOLD.(curAnimal){1}=curAnimalBOLD;
end

% Important Note (AK): Remove 2 experiments with sampling problems:
allBOLD.A09GB1{1}(:,:,2:3)=[];
end