% Reads all subject data into a single structure
% Author: Ali Karimi <ali.karimi@brain.mpg.de>

dataDir='C:\Users\karimia\code\mvgc_v1.0\Data';
Dirs=dir(dataDir);
thisData=struct();
allData=struct();
for i=3:length(Dirs)
    expName=Dirs(i).name;
    dataFile=matfile(fullfile(dataDir,expName,...
        'All_individual_mean_timecourses.mat'));
    thisData=dataFile.Statistics;
    allData.(expName)=thisData.(expName);
end

save(fullfile(dataDir,'allData.mat'),'allData');