function [emptyTable] = createEmptyTable(dataType,...
    variableNames,rowNames)
% CREATEEMPTYTABLE create an empty table used for outputs in various
% methods

% Author: Ali Karimi <ali.karimi@brain.mpg.de>

tableSize=[length(rowNames),length(variableNames)];

switch dataType
    case 'cell'
        array=cell(tableSize);
        emptyTable=cell2table(array,'VariableNames',...
            variableNames,'RowNames',...
            rowNames);
    case 'double'
        array=zeros(tableSize);
        emptyTable=array2table(array,'VariableNames',...
            variableNames,'RowNames',...
            rowNames);
    otherwise
        error('dataType not valid')
end

end


