paths=util.getPaths;
addpath(genpath('spm12'))
roiDir=fullfile(paths.RD,'A14AQ1','VOIs');
files=dir(fullfile(roiDir,'*.mat'));
rois=cell(size(files));
for i=1:length(files)
    rois{i}=load(fullfile(files(i).folder,files(i).name));
end