function [paths] = getPaths()
%GETPATHS 
mainDir = '/Users/karimia/code/mvgc_grangercausality';
paths.dataDir = fullfile(mainDir,'Data');
paths.RD=fullfile(paths.dataDir,'RectalDistension');
paths.testAnimal=fullfile(paths.RD,'A14AQ1');
paths.DCM=fullfile(paths.testAnimal,'DCM');

paths.fig = fullfile(mainDir,'Figs');
end

