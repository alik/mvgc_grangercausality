function[ newEpochOrder] = TrialRoiAvgTimeCourse(Statistics, R)
% GETROIAVGTIMECOURSE outputs the timecourses, the average of timecourses

% Extract Info from 'Statistics' Structure
field = fieldnames(Statistics)';
subfields = fieldnames(Statistics.(field{1}))';
ROI = fieldnames(Statistics.(field{1}).(subfields{1}))';

numberOftrials = 15;

%% Pull out the relevant timecourse information

% Epoch from 1 - 30 seconds
initialIndex = 1:30;
Trial_timecourse_firstEpoch = zeros(length(initialIndex),numberOftrials,length(subfields));

for sJ = 1:length(subfields)
    for epoch=1:numberOftrials
        stepSize=(40*(epoch-1));
        curIndex=stepSize+initialIndex;
        Trial_timecourse_firstEpoch(:,epoch, sJ) = Statistics.(field{1}). ...
            (subfields{sJ}).(ROI{R}).statistic(curIndex,2);
    end
end

% Epoch from 31 - 35 seconds
initialIndex2 = 31:35;
Trial_timecourse_secondEpoch = zeros(length(initialIndex2),numberOftrials,length(subfields));
for sJ = 1:length(subfields)
    for epoch=1:numberOftrials
        stepSize=(40*(epoch-1));
        curIndex2=stepSize+initialIndex2;
        Trial_timecourse_secondEpoch(:,epoch, sJ) = Statistics.(field{1}). ...
            (subfields{sJ}).(ROI{R}).statistic(curIndex2,2);
    end
end

% Order epochs
newEpochOrder = cat(1, Trial_timecourse_secondEpoch, Trial_timecourse_firstEpoch);

end
