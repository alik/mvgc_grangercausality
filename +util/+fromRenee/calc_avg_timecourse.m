function calc_avg_timecourse(modality, subj4struct, ROIs, sessionType)
% This function calculates the average timecourse for specific ROIs
% ROIs defined anatomically, but script can be modified for compatibility
% with functional ROIs
%
% Compiler(s): Renee Hartig (renee.hartig@tuebingen.mpg.de) & Ali Karimi
% (ali.karimi@brain.mpg.de)
%
% Inputs:
%
% modality % string containing modality information, e.g. 'RD', 'taste',
% 'temp', 'EL', 'tVNS'
%
% subj4struct % string containting experiment ID without period, no spacing, e.g. 'A09GB1'
%
% ROIs % Enter 1 for insula, 2 for thalamus, 3 for subcortical-cortical,
% 4 for hypothesis left, or 5 for hypothesis right

% sessionType = input('Enter 1 for all sessions, 2 for the first half or 3 for the second half:');
clearvars -except modality subj4struct ROIs sessionType
%% Defining Analysis Mode
fprintf('Analyzing the mean BOLD signal of anatomical ROIs');
ana_ROIs = 1;

% stats = string input indicating mean or median
stats = 'mean';

% User-Determined Inputs
% ROIs = input(Enter 1 for insula, 2 for thalamus, 3 for subcortical-cortical,
% 4 for hypothesis left, or 5 for hypothesis right);
% sessionType = input('Enter 1 for all sessions, 2 for the first half or 3 for the second half:');

if ROIs == 1
    % Register Insula ROInumbers
    ROInumbers = [16:18, 35, 48:49, 65:67, 84, 97:98];
    ROIlocation = 'Insula';
elseif ROIs == 2
    % Register Thalamus ROInumbers
    ROInumbers = [3, 19:20, 23:25, 33, 43:46, 52]; %, 68:69, 72:74, 82, 92:95];
    ROIlocation = 'Thalamus';
elseif ROIs == 3
    % Register Hypothesis Drive Right ROInumbers
    ROInumbers = [50:51, 58, 61:62, 64, 71, 74:75, 79, 85, 94];
    ROIlocation = 'Hypothesis_Driven_Right';
elseif ROIs == 4
    % Register Hypothesis Drive Left ROInumbers
    ROInumbers = [1:2, 9, 12:13, 15, 22, 25:26, 30, 36, 45];
    ROIlocation = 'Hypothesis_Driven_Left';
elseif ROIs == 5
    % Register Extraneous Activity Right ROInumbers
    ROInumbers = [52, 54, 59, 63, 86, 73, 70, 82, 79, 72, 70, 69];
    ROIlocation = 'Controls_Right';
elseif ROIs == 6
    % Register Extraneous Activity Left ROInumbers
    ROInumbers = [3, 5, 10, 14, 37, 24, 21, 33, 30, 23, 21, 20];
    ROIlocation = 'Controls_Left';
else ROIs == 7
    % Register Subcortical-Cortical ROInumbers
    ROInumbers = [84, 35, 66, 17, 64, 15, 51, 2, 65, 16, 97, 48, ...
        98, 49, 67, 18, 94, 45, 85, 36, 86, 37, 71, 22, 61, 12, 50, 1, ...
        62, 13, 75, 26, 80, 31, 74, 25, 82, 33];
    ROIlocation = 'Subcortical_Cortical';
end


if sessionType == 1
    sessionType = 'All';
elseif sessionType == 2
    sessionType = 'First_Half';
else sessionType == 3
    sessionType = 'Second_Half';
end

%% Establish Workspace
clearvars -except modality subj4struct analysisNum ana_ROIs stats ...
    ROIlocation ROInumbers sessionType ROIs

% Locate .mat file to load containing Statistics structure
if ana_ROIs <= 0
    analysisNum = input('What is the analysis number?', 's');
    matFileDir = strcat('D:\NHP_fMRI\results\', num2str(modality), '\', num2str(subj4struct), ...
        '\', num2str(analysisNum), '\ROIs');
else
    matFileDir = strcat('D:\NHP_fMRI\results\', num2str(modality), '\', num2str(subj4struct), ...
        '\ana_custom_ROIs');
end
cd(matFileDir);

load(strcat(sessionType, '_individual_', stats, '_timecourses.mat'));
load('onsets.mat');

%% Extract Info from 'Statistics' Structure
field = fieldnames(Statistics)';
subfields = fieldnames(Statistics.(field{1}))';
ROI = fieldnames(Statistics.(field{1}).(subfields{1}))';

%% Overlay individual session timecourses for a particular ROI
% for R = 1:length(ROInumbers)
%     figure;
%     for sJ = 1:length(subfields)
%         %         clear('timecourses', 'x', 'e', 'ROI');
%         [filtb,filta] = butter(2, [0.008 .25]);
%         timecourses = zscore(filtfilt(filtb, filta, (Statistics.(field{1}).(subfields{sJ}).(ROI{R}).statistic(1:end,2))));
%         x = Statistics.(field{1}).(subfields{sJ}).(ROI{R}).statistic(:,1);
%         % Plot with standard deviation
%         avg_timecourses = mean(timecourses,2)';
%         shadedErrorBar(x, timecourses, avg_timecourses, 'lineprops', '-o','transparent',1);
%         % Less preferred way of displaying error bars
%         % errorbar(x, timecourses, e, 'vertical');
%     end
%     
%     % With LGN coil
%     patch([onsets{1,1}/2; onsets{1,1}/2+5; onsets{1,1}/2+5; onsets{1,1}/2],...
%         repmat([0;0; 145;145], 1, length(onsets{1,1})) ,...
%         'g', 'FaceAlpha',.1, 'LineStyle', ':');
%     patch([onsets{1,2}/2; onsets{1,2}/2+2; onsets{1,2}/2+2; onsets{1,2}/2], ...
%         repmat([0;0; 145;145], 1, length(onsets{1,1})) ,...
%         'b', 'FaceAlpha',.1, 'LineStyle', ':');
%     
%     % With custom helmet coil
%     % patch([onsets{1,1}; onsets{1,1}+10; onsets{1,1}+10; onsets{1,1}], ...
%     %     repmat([0;0; 145;145], 1, length(onsets{1,1})), ...
%     %     'g', 'FaceAlpha',.1, 'LineStyle', ':');
%     % patch([onsets{1,2}; onsets{1,2}+4; onsets{1,2}+4; onsets{1,2}], ...
%     %     repmat([0;0; 145;145], 1, length(onsets{1,1})) , ...
%     %     'b', 'FaceAlpha',.1, 'LineStyle', ':');
%     
%     legend({num2str(ROI{R}), 'STD'}, 'Location','northeastoutside', 'Interpreter', 'none');
%     title('Individual ROI BOLD Response');
%     ylabel('Mean BOLD Response');
%     ylim([-5 5]);
%     xlabel('Timecourse (sec)');
% end
% close all

%% Take the average timecourse for a particular ROI across sessions

if ROIs == 7
layout = [19, 2];
else
layout = [4,3];
end

h=figure();
sizeXY=[17+2,7+2].*layout;
set(h, 'Units','centimeters', 'Position',[0 0 sizeXY(1) sizeXY(2)]);

RoiIdx=1;
color_D=distinguishable_colors(length(ROInumbers));
avg_timecourses=[];

for R = ROInumbers
    % Get statistics
    [ timecourses,avg_timecourses(:,RoiIdx),x,e ] = ...
        getSingleSessionRoiAvgTimeCourse(Statistics,R);
    % Plotting
    subplot(layout(1),layout(2),RoiIdx)
    
    shadedErrorBar(x, avg_timecourses(:,RoiIdx), e, 'lineprops',{'Color',color_D(RoiIdx,:)},'transparent',1);
    
    % Lay down the stim lines
%     patch([onsets{1,1}; onsets{1,1}+10; onsets{1,1}+10; onsets{1,1}], ...
%         repmat([-2;2; -2;2], 1, length(onsets{1,1})), 'g', 'FaceAlpha',.1, 'LineStyle', ':');
%     patch([onsets{1,2}; onsets{1,2}+4; onsets{1,2}+4; onsets{1,2}], ...
%         repmat([-2;2; 2;2], 1, length(onsets{1,1})) , ...
%         'b', 'FaceAlpha',.1, 'LineStyle', ':');
    
%     patch([onsets{1,1}; onsets{1,1}+10; onsets{1,1}+10; onsets{1,1}], ...
%         repmat([-2;2; -2;2], 1, length(onsets{1,1})), 'g', 'FaceAlpha',.1, 'LineStyle', ':');
%     patch([onsets{1,2}; onsets{1,2}+4; onsets{1,2}+4; onsets{1,2}], ...
%         repmat([-2;2; 2;2], 1, length(onsets{1,1})) , ...
%         'b', 'FaceAlpha',.1, 'LineStyle', ':');

    legend({num2str(ROI{R}), 'SEM'}, 'Location','northeastoutside', 'Interpreter', 'none');
    
    title('Individual ROI BOLD Response');
    ylabel('Mean BOLD Response');
    ylim([-1.5 1.5]);
    xlabel('Timecourse (sec)');
    
    % Counter for suplotting indices
    RoiIdx=RoiIdx+1;
end

% Save figure in relevant folder
cd(strcat('D:\NHP_fMRI\results\', modality, '\', num2str(subj4struct), '\ana_custom_ROIs'))
mkdir('Avg_ROI_Timecourse');
cd('Avg_ROI_Timecourse');
save(strcat(ROIlocation, '_avg_timecourses'), 'avg_timecourses');
print(strcat(sessionType, '_', ROIlocation, '_', stats, '_', 'Avg_ROI_Timecourse'), '-dsvg');
end
