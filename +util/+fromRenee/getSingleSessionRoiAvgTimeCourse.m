function [timecourses,avg_timecourses,x,e] = getSingleSessionRoiAvgTimeCourse(Statistics,R)
%GETROIAVGTIMECOURSE outputs the timecourses, the average of timecourses

%over sessions and the x and error values for plotting
field = fieldnames(Statistics)';
subfields = fieldnames(Statistics.(field{1}))';
ROI = fieldnames(Statistics.(field{1}).(subfields{1}))';

[filtb,filta] = butter(2, [0.008 .25]);

for sJ = 1:length(subfields)
         timecourses(:,sJ) = zscore(filtfilt(filtb, filta, (Statistics.(field{1}).(subfields{sJ}).(ROI{R}).statistic(1:600,2))));
end

% Take the average over sessions of this single ROI
avg_timecourses = nanmean(timecourses,2)';

% Get the x(timestamps) and the error values 
% x = Statistics.(field{1}).(subfields{sJ}).(ROI{R}).statistic(:,1);
tstep = 1;
x = [1:tstep:600]';

% Standard deviation (SEM)
e = std(timecourses,[],2)./sqrt(size(timecourses,1));
% e=transpose(prctile(timecourses,[25 75],2));

end
