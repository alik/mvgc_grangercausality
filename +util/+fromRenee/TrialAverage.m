function TrialAverage(modality, ROIs, sessionType)
% This function calculates the average trial timecourse for specific ROIs
% ROIs defined anatomically

% Compiler(s): Renee Hartig (renee.hartig@tuebingen.mpg.de) & Ali Karimi
% (ali.karimi@brain.mpg.de)
%
% Inputs:
%
% modality % string containing modality information, e.g. 'RD', 'taste',
% 'temp', 'EL', 'tVNS'
%
% ROIs % Enter 1 for insula, 2 for thalamus, 3 for subcortical-cortical,
% 4 for hypothesis left, or 5 for hypothesis right

% sessionType = input('Enter 1 for all sessions, 2 for the first half or 3 for the second half:');

clearvars -except modality sessionType ROIs
%% Defining Analysis Mode
fprintf('Calculating the mean trial epoch');

% User-Determined Inputs
% ROIs = input(Enter 1 for insula, 2 for thalamus, 3 for subcortical-cortical,
% 4 for hypothesis left, or 5 for hypothesis right);
% sessionType = input('Enter 1 for all sessions, 2 for the first half or 3 for the second half:');

if ROIs == 1
    % Register Insula ROInumbers
    ROInumbers = [16:18, 35, 48:49, 65:67, 84, 97:98]
    ROIlocation = 'Insula';
elseif ROIs == 2
    % Register Thalamus ROInumbers
    ROInumbers = [3, 19:20, 23:25, 33, 43:46, 52]; %, 68:69, 72:74, 82, 92:95];
    ROIlocation = 'Thalamus';
elseif ROIs == 3
    % Register Hypothesis Drive Right ROInumbers
    ROInumbers = [50:51, 58, 61:62, 64, 71, 74:75, 79, 85, 94];
    ROIlocation = 'Hypothesis_Driven_Right';
elseif ROIs == 4
    % Register Hypothesis Drive Left ROInumbers
    ROInumbers = [1:2, 9, 12:13, 15, 22, 25:26, 30, 36, 45];
    ROIlocation = 'Hypothesis_Driven_Left';
elseif ROIs == 5
    % Register Extraneous Activity Right ROInumbers
    ROInumbers = [52, 54, 59, 63, 86, 73, 70, 82, 79, 72, 70, 69];
    ROIlocation = 'Controls_Right';
elseif ROIs == 6
    % Register Extraneous Activity Left ROInumbers
    ROInumbers = [3, 5, 10, 14, 37, 24, 21, 33, 30, 23, 21, 20];
    ROIlocation = 'Controls_Left';
else ROIs == 7
    % Register Subcortical-Cortical ROInumbers
%     ROInumbers = [66, 17, 64, 15, 51, 2, 65, 16, 97, 48, ...
%         98, 49, 67, 18, 94, 45, 85, 36, 86, 37, 71, 22, 50, 1, ...
%         62, 13, 75, 26, 80, 31, 74, 25];
%     ROIlocation = 'Subcortical_Cortical';

    % For RD Figure
    ROInumbers = [1 51 14 64 65 18 23 25 26 80 85 94]
    ROIlocation = 'L_RD_ROI_Analysis';
end

if sessionType == 1
    sessionType = 'All';
elseif sessionType == 2
    sessionType = 'First_Half';
else sessionType == 3
    sessionType = 'Second_Half';
end

%% Take the average timecourse for a particular ROI across trials

% Set plot layout - contingent upon number of ROIs
if length(ROInumbers) == 12
layout = [4,3];
else
layout = [8,4];
end

h=figure();
sizeXY=[10+2,7+2].*layout;
set(h, 'Units','centimeters', 'Position',[0 0 sizeXY(1) sizeXY(2)]);

avgTimepoint=[];

color_D = distinguishable_colors(length(ROInumbers));
numberOfMonkeys = 10;
Trial_timecourses=cell(length(ROInumbers),numberOfMonkeys);

% Load ROI and experiment data
load('D:\NHP_fMRI\results\ROI');
cd(strcat('D:\NHP_fMRI\results\RD_extra\group\Trial_epoch'));
load('subj4struct');

monkeyIdx=1;
for statName = subj4struct'
    load(statName{1});
    Statistics=eval(statName{1});
    RoiIdx = 1;
    for R = ROInumbers
        % Get statistics
        [newEpochOrder{RoiIdx,monkeyIdx}] = TrialRoiAvgTimeCourse(Statistics, R);
        RoiIdx=RoiIdx+1;
    end
    monkeyIdx=monkeyIdx+1;
end

%% Now reshape data so that each cell becomes the following size:
% (number of data points x (numberOfSessions x numberOfEpochsInEachSession)
% of dat
numberOfDataPointsIn1Trial=size(newEpochOrder{1},1);
timecoursesReshaped=cellfun(@(x) reshape(x,numberOfDataPointsIn1Trial,[]),...
    newEpochOrder,'UniformOutput',false);

% Concatenate the results
for roi=1:size(timecoursesReshaped,1)
    timeCourse_MonkeysCombined{roi}=cat(2,timecoursesReshaped{roi,:});
    % Set bandpass filter
    [filtb,filta] = butter(2, [.008 0.25]);
    NormBand_timecourse{1,roi} = zscore(filtfilt(filtb, filta, (timeCourse_MonkeysCombined{roi})));
end

% Get the mean of trials over the subjects
averageTrials=cellfun(@(x)nanmean(x,2),NormBand_timecourse,...
    'UniformOutput',false);

averageTrials = cell2mat(averageTrials);

% Standard deviation
% concatonation for err calc
for i = 1:12
    errAvgTimepoint(:,i) = averageTrials(:,i);
end

e = std(errAvgTimepoint,[],2)./sqrt(size(newEpochOrder,1));
% e=transpose(prctile(timecourses,[25 75],2));

% Get the x-axis (time interval)
x = -4:30;

%% Plot the results
RoiIdx = 1;
for R = ROInumbers
    
    subplot(layout(1),layout(2),RoiIdx)
    
    shadedErrorBar(x, averageTrials(:,RoiIdx), e, ...
        'lineprops',{'Color',color_D(RoiIdx,:)},'transparent',1);
    
    % Plot Accessories
    patch([0,7, 10, 10], [-0.2,0.2, 0.2, -0.2], ...
        'g', 'FaceAlpha',.1, 'LineStyle', ':');
    patch([10, 10, 11, 14], [-0.2,0.2, 0.2, -0.2], ...
        'p', 'FaceAlpha',.1, 'LineStyle', ':');
    
    % Old way (rectangular pulse stim lines)
    %     patch([5; 7+2; 9+1; 5], repmat([-1;-1; 1;1], 1) , ...
    %             'g', 'FaceAlpha',.1, 'LineStyle', ':');
    %         patch([15; 15+4; 15+4; 15], repmat([-1;-1; 1;1], 1) , ...
    %             'p', 'FaceAlpha',.1, 'LineStyle', ':');
    
    legend({'SEM',ROI{R}}, 'Location','northeastoutside',...
        'Interpreter', 'none');
    title('Average Trial Response');
    ylabel('Mean BOLD Response');
    ylim([-0.15 0.15]);
    xlim([-4 30]);
    xlabel('Time (sec)');
    
    % Counter for suplotting indices
    RoiIdx = RoiIdx +1;
    
end

% Save figure in relevant folder
cd(strcat('D:\NHP_fMRI\results\RD_extra\group\Trial_epoch\'))
print(strcat(sessionType, '_', ROIlocation, '_', 'Trial_Epoch'), '-dsvg');
end
