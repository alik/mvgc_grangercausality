function[ allROIs,avg_allROIs,x,e ] = getSessionAllRoiAvgTimeCourse( Statistics,ROInumbers,folders2analyze)
%GETROIAVGTIMECOURSE outputs the timecourses, the average of timecourses
%over sessions and the x and error values for plotting

field = fieldnames(Statistics)';
subfields = fieldnames(Statistics.(field{1}))';

for sJ = 1:length(folders2analyze)
    rIdx=1;
    for R = ROInumbers
    ROI = fieldnames(Statistics.(field{1}).(subfields{sJ}))';
    [filtb,filta] = butter(2, [0.0333 .2]);
    allROIs(:,sJ,rIdx) = zscore(filtfilt(filtb, filta, (Statistics.(field{1}).(subfields{sJ}).(ROI{R}).statistic(1:end,2))));
    rIdx=rIdx+1;
    end
end

% Take the average over sessions of this single ROI
avg_allROIs = mean(allROIs,3)';
% Get the x(timestamps) and the error values
x = Statistics.(field{1}).(subfields{sJ}).(ROI{R}).statistic(:,1);
% Standard deviation
e = std(allROIs,[],3)./sqrt(size(allROIs,1));
e=transpose(prctile(allROIs,[25 75],2));
