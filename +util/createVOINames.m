function [voiNames] = createVOINames(voiDir,VOIs,sessionNr)
%CREATEVOINAMES
voiNames=cell(size(VOIs));
folderNames = cellfun(@(x) strrep(x,'VOI_',''),VOIs,'uni',0);

for i=1:length(VOIs)
    voiNames{i}=fullfile(voiDir,folderNames{i},...
        [VOIs{i},'_',num2str(sessionNr)]);
end
end

