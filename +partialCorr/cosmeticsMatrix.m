function [hStrings] = cosmeticsMatrix(pCorrelation,colorLim)
%ADDNUMBERS2MATRIX Write text values in the connectivity matrix of regions
% Colormap
data = pCorrelation (:);
L = length(data);
% Calculate where proportionally indexValue lies between minimum and
% maximum values
largest = max(colorLim);
smallest = min(colorLim);
indexValue = 0;     % value for which to set a particular color
index = L*abs(indexValue-smallest)/(largest-smallest);
% Color Edges
topColor = [0 0 0];         % color for maximum data value (red = [1 0 0])
indexColor = [1 1 1];       % color for indexed data value (white = [1 1 1])
bottomcolor = [abs(smallest) 0 0]*6;      % color for minimum data value (blue = [0 0 1])

% Create color map ranging from bottom color to index color
% Multipling number of points by 100 adds more resolution
customCMap1 = [linspace(bottomcolor(1),indexColor(1),100*index)',...
            linspace(bottomcolor(2),indexColor(2),100*index)',...
            linspace(bottomcolor(3),indexColor(3),100*index)'];
% Create color map ranging from index color to top color
% Multipling number of points by 100 adds more resolution
customCMap2 = [linspace(indexColor(1),topColor(1),100*(L-index))',...
            linspace(indexColor(2),topColor(2),100*(L-index))',...
            linspace(indexColor(3),topColor(3),100*(L-index))'];
customCMap = [customCMap1;customCMap2];  % Combine colormaps
colormap(customCMap)
colorbar;
% The text in the figure

textStrings = num2str(pCorrelation(:), '%0.2f');       % Create strings from the matrix values
textStrings = strtrim(cellstr(textStrings));  % Remove any space padding
[x, y] = meshgrid(1:length(pCorrelation));  % Create x and y coordinates for the strings
hStrings = text(x(:), y(:), textStrings(:), ...  % Plot the strings
                'HorizontalAlignment', 'center');
midValue = largest/2;  % Get the middle value of the color range
textColors = repmat(pCorrelation(:) > midValue, 1, 3);  % Choose white or black for the
                                               %   text color of the strings so
                                               %   they can be easily seen over
                                               %   the background color
set(hStrings, {'Color'}, num2cell(textColors, 2));  % Change the text colors% Figure properties
set(hStrings,'FontSize',6);
end

