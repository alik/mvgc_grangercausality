% Models 1:4 as drawn in:
%/home/alik/code/mvgc_grangercausality/Data/RectalDistension/A14AQ1/DCM/04072019/Models.jpeg
% This script generates the DCM files from example DCM files of one session
% To generate the first session files I use the GUI as explained in DCM
% chapter of the SPM Manual 12
% After running this script the models are compared again using the gui
%--------------------------------------------------------------------------
paths=util.getPaths;
data_path=fullfile(paths.testAnimal,'14');
curDCMPath=fullfile(paths.DCM,'14092019');
p.numModels=4;p.numSessions=8;
p.nameGen=@(mod,sess)sprintf('DCM_model%0.1u_sess%0.1u.mat',mod,sess);

% Get the VOI names for all sessions except the first one
voiPath=fullfile(paths.testAnimal,'Eigenvariates');
voiNames=cell(7,1);
VOIstubs={'VOI_R_Idfm_Idfp','VOI_R_Idys','VOI_R_vAIC','VOI_R_dAIC',...
    'VOI_L_Idfm_Idfp','VOI_L_Idys','VOI_L_vAIC','VOI_L_dAIC'};
for sess=2:8
    voiNames{sess} = util.createVOINames(voiPath,...
        VOIstubs,sess);
end

% Load the models created in the gui
firstSessDCM=cell(p.numModels, 1);
for m=1:p.numModels
    firstSessDCM {m}=load(fullfile(curDCMPath,...
        p.nameGen(m,1)));
end

% create dcm model files for each session separately
clear DCM
for sess=2:p.numSessions
    for m=1:p.numModels
        DCM=spm_dcm_voi...
            (firstSessDCM{m}.DCM,voiNames{sess});
        fname=fullfile(curDCMPath,p.nameGen(m,sess));
        save(fullfile(curDCMPath,p.nameGen(m,sess)),'DCM');
        clear DCM;
    end
end
%% Inference
clear matlabbatch
% Get filenames of the DCM files to run the inference on
fnames=dir(fullfile(curDCMPath,'DCM*.mat'));
fnames={fnames.name};
% Add dir
fnames=cellfun(@(x)fullfile(curDCMPath,x),fnames,'UniformOutput',false);
matlabbatch{1}.spm.dcm.fmri.estimate.dcmmat = fnames;

spm_jobman('run',matlabbatch);
