% Model name DCM_DysG2AG: U-->R/L Idys-->R/L vAIC
% DCM_AG2DysG: U-->R/L vAIC-->R/L Idys
% echo time: 0.04, bilinear, one state per region no stochasticity and centre input include inflation, deflation and rest as input
% DCM Estimation
%--------------------------------------------------------------------------
paths=util.getPaths;
data_path=fullfile(paths.RD,'A14AQ1','14');

% Get the VOI names for all sessions except the first one
voiPath=fullfile(paths.RD,'A14AQ1','VOIs');
voiNames=cell(7,1);
for sess=2:8
voiNames{sess}=util.createVOINames(voiPath,...
    {'VOI_R_Idys','VOI_L_Idys','VOI_R_vAIC','VOI_L_vAIC'},sess);
end
% Load the models created in the gui
firstSessDCM.fwd=load(fullfile(data_path,'DCM_DysG2AG_1'));
firstSessDCM.bwd=load(fullfile(data_path,'DCM_AG2DysG_1'));
modelNames={'DCM_DysG2AG_','DCM_AG2DysG_'};
modelAcronyms={'fwd','bwd'};
% create dcm model files for each session separately
clear DCM
for sess=2:8
    for m=1:2
    DCM=spm_dcm_voi...
        (firstSessDCM.(modelAcronyms{m}).DCM,voiNames{sess});
    fname=fullfile(data_path,[modelNames{m},num2str(sess),'.mat']);
    save(fullfile(data_path,[modelNames{m},num2str(sess),'.mat']),'DCM');
    end
end
clear matlabbatch
fnames=dir('./*.mat');
fnames={fnames.name};
matlabbatch{1}.spm.dcm.fmri.estimate.dcmmat = fnames;

spm_jobman('run',matlabbatch);

% Bayesian Model Comparison
%--------------------------------------------------------------------------
DCM_bwd = load(fullfile(data_path,'GLM','DCM_mod_bwd.mat'),'F');
DCM_fwd = load(fullfile(data_path,'GLM','DCM_mod_fwd.mat'),'F');
fprintf('Model evidence: %f (bwd) vs %f (fwd)\n',DCM_bwd.F,DCM_fwd.F);
